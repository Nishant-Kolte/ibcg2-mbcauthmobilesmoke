Feature: MBC(Auth) Mobile smoke test script

  @Smoke @MBCMobile
  Scenario: To verify user is able to login to MBC(auth) mobile successfully
    Given   User try to login to MBCauth mobile via valid credentials
    Then  User should get logged in successfully and land on mbc mobile dashboard page

  @Smoke @MBCMobile
  Scenario: To verify Your Profile (Security Settings) page for MBC ppt
    When   User clicks on security settings link
    Then  Verify User gets redirected to Your Profile Page for MBC ppt

    @Smoke @MBCMobile
  Scenario: To verify user is able to logout successfully from MBC mobile
    When   User clicks on Logout link on MBC dashboard
    Then  User should able to logout successfully from MBC mobile

