Feature: IBCG2 Mobile smoke test script

  @Smoke @IBCG2Mobile
  Scenario: To verify user is able to login to IBCG2 mobile successfully
    Given   User try to login to IBCG2 mobile via valid credentials
    Then  User should get logged in successfully and land on mobile landing page

  @Smoke @IBCG2Mobile
  Scenario: To verify successful SSO to HB
    Given   User should get logged in successfully and land on mobile landing page
    Then  Verify User is able to SSO to HB successfully

#  @Smoke @IBCG2Mobile
 # Scenario: To verify Your Profile link
  #  When   User clicks on Your Profile button
   # Then  Verify User gets redirected to Your Profile Page

  @Smoke @IBCG2Mobile
  Scenario: To verify Help link
    When   User clicks on Help button
    Then  Verify User gets redirected to Help Page

  @Smoke @IBCG2Mobile
  Scenario: To verify Contact Us link
    When   User clicks on Contact Us button
    Then  Verify User gets redirected to Contact Us Page

  @Smoke @IBCG2Mobile
  Scenario: To verify Privacy link
    When   User clicks on Privacy link
    Then  Verify User gets redirected to Privacy Policy Page

  @Smoke @IBCG2Mobile
  Scenario: To verify Terms link
    When   User clicks on Terms link
    Then  Verify User gets redirected to Terms of use Page


  @Smoke @IBCG2Mobile
  Scenario: To verify user is able to logout successfully from IBCG2 mobile
    When   User clicks on Logout button
    Then  User should able to logout successfully from IBCG2 mobile

  @Smoke @IBCG2Mobile
  Scenario: To verify Full Website link
    When   User clicks on Full Website link
    Then  Verify User gets redirected to desktop IBCG2 Page