var data = require('../../TestResources/MBCAuthMobileData.js');
var Objects = require(__dirname + '/../../repository/MBCAuthMobilePages.js');
var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');
//var email = require(__dirname + '/../../node_modules/emailjs');

var LoginPage,DashboardPage,LogoutPage,YourProfilePage;

function initializePageObjects(client, callback) {
    browser = client;
    var MBCAuthMobilePage = browser.page.MBCAuthMobilePages();
    page = MBCAuthMobilePage.section;
    LoginPage= MBCAuthMobilePage.section.LoginPage;
    DashboardPage= MBCAuthMobilePage.section.DashboardPage;
    LogoutPage= MBCAuthMobilePage.section.LogoutPage;
    YourProfilePage=MBCAuthMobilePage.section.YourProfilePage;
    callback();
}

module.exports = function() {

this.Given(/^User try to login to MBCauth mobile via valid credentials$/, function () {
        var URL;
        browser = this;

        if (data.TestingEnvironment == "QAI") {
            URL = data.urlQAI;
            var userDB = data.userQAI;
        }
        else if (data.TestingEnvironment == "CITEST") {
            URL = data.urlCITEST;
            var userDB = data.userCITEST;
        }
        else if (data.TestingEnvironment == "CSO") {
            URL = data.urlCSO;
            var userDB = data.userCSO;
        }
        else
        {
            URL = data.urlPROD;
            var userDB = data.userPROD;
        }

        //Login scenario
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
             //   LoginPage.waitForElementVisible('@MBCHeader', data.averagewaittime)
                LoginPage.waitForElementVisible('@LoginToYourAccount', data.averagewaittime)
                    .waitForElementVisible('@Username', data.averagewaittime)
                    .waitForElementVisible('@Password', data.averagewaittime)
                    .waitForElementVisible('@Enter', data.averagewaittime)
                    .setValue('@Username',userDB.username)
                    .setValue('@Password',userDB.password)
                    .click('@Enter');

            });


        });

    this.Then(/^User should get logged in successfully and land on mbc mobile dashboard page$/, function () {
        browser= this;
        browser.pause(10000);
              initializePageObjects(browser, function () {
                    DashboardPage.waitForElementVisible('@WelcomeMsg', data.averagewaittime)
                                .waitForElementVisible('@LogoutLink', data.averagewaittime);
              });
                console.log("logged in successfully and MBC mobile dashboard page is displayed!");

        });


    this.When(/^User clicks on security settings link$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            DashboardPage.waitForElementVisible('@SecuritySettingsLink1', data.averagewaittime)
                .click('@SecuritySettingsLink1');
browser.pause(10000);
            DashboardPage.waitForElementVisible('@SecuritySettingsLink2', data.averagewaittime)
                .click('@SecuritySettingsLink2');
        });
    });
    this.Then(/^Verify User gets redirected to Your Profile Page for MBC ppt$/, function () {
        browser = this;
        browser.window_handles(function(result) {
            var new_window = result.value[1];
            var parent_window = result.value[0];
            browser.switchWindow(new_window);
            initializePageObjects(browser, function () {

                YourProfilePage.waitForElementVisible('@PersonalInformation', data.averagewaittime)
                    .waitForElementVisible('@LoginInformation', data.averagewaittime)
                    .waitForElementVisible('@SecurityQuestions', data.averagewaittime)

            });

            browser.closeWindow();
            browser.switchWindow(parent_window);
        });
    });

    this.When(/^User clicks on Logout link on MBC dashboard$/, function () {
        browser = this;
        browser.pause(5000);
        initializePageObjects(browser, function () {
            DashboardPage.waitForElementVisible('@Dashboardlink', data.averagewaittime)
                .click('@Dashboardlink');
            DashboardPage.waitForElementVisible('@LogoutLink', data.averagewaittime)
                .click('@LogoutLink');

        });
    });

    this.Then(/^User should able to logout successfully from MBC mobile$/, function () {
         browser = this;
            initializePageObjects(browser, function () {

                LogoutPage.waitForElementVisible('@LogoutSuccess',data.averagewaittime);
                LogoutPage.waitForElementVisible('@LogInButton',data.averagewaittime)
                .click('@LogInButton');
            });

    })

};
