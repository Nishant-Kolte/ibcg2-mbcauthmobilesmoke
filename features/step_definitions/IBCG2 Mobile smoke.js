var data = require('../../TestResources/IBCG2MobileData.js');
var Objects = require(__dirname + '/../../repository/IBCG2MobilePages.js');
var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');
//var email = require(__dirname + '/../../node_modules/emailjs');

var LoginPage,LandingPage,LogoutPage,ProfilePage;

function initializePageObjects(client, callback) {
    browser = client;
    var IBCG2Mobilepage = browser.page.IBCG2MobilePages();
    page = IBCG2Mobilepage.section;
    LoginPage= IBCG2Mobilepage.section.LoginPage;
    LandingPage= IBCG2Mobilepage.section.LandingPage;
    LogoutPage= IBCG2Mobilepage.section.LogoutPage;
    ProfilePage=IBCG2Mobilepage.section.ProfilePage;
    callback();
}

module.exports = function() {

this.Given(/^User try to login to IBCG2 mobile via valid credentials$/, function () {
        var URL;
        browser = this;

        if (data.TestingEnvironment == "QAI") {
            URL = data.urlQAI;
            var userDB = data.userQAI;
        }
        else if (data.TestingEnvironment == "CITEST") {
            URL = data.urlCITEST;
            var userDB = data.userCITEST;
        }
        else if (data.TestingEnvironment == "CSO") {
            URL = data.urlCSO;
            var userDB = data.userCSO;
        }
        else if (data.TestingEnvironment == "CIDEV") {
            URL = data.urlCIDEV;
            var userDB = data.userCIDEV;
        }
        else
        {
            URL = data.urlPROD;
            var userDB = data.userPROD;
        }

        //Login scenario
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPage.waitForElementVisible('@BenefitCentralHeader', data.averagewaittime)
                    .waitForElementVisible('@LoginToYourAccount', data.averagewaittime)
                    .waitForElementVisible('@Username', data.averagewaittime)
                    .waitForElementVisible('@Password', data.averagewaittime)
                    .waitForElementVisible('@Enter', data.averagewaittime)
                    .setValue('@Username',userDB.username)
                    .setValue('@Password',userDB.password)
                    .click('@Enter');

            });


        });

    this.Then(/^User should get logged in successfully and land on mobile landing page$/, function () {
        browser= this;

        browser.url(function (current_url) {
            var pageurl1 = current_url.value;
            var pageurl = pageurl1.toUpperCase();
            if (pageurl.includes("HB-")) {
                initializePageObjects(browser, function () {
                    LandingPage.waitForElementVisible('@CurrentCoverage', data.averagewaittime) });
                console.log("logged in successfully and HB page is displayed!");

            }
            else if(pageurl.includes("TWOTOBHOME")) {
                LandingPage.waitForElementVisible('@TwotobHome', data.averagewaittime)
                console.log("logged in successfully and TwoTobHome page is displayed!");
            }
            else
            {
                LandingPage.waitForElementVisible('@OutagePage', data.averagewaittime)
                console.log("logged in successfully but mobile Outage page is displayed!");

            }
            });

        });

    this.Then(/^Verify User is able to SSO to HB successfully$/, function () {
       browser=this;
        browser.url(function (current_url) {
            var pageurl = current_url.value
            var pageurl = pageurl.toUpperCase();
            if (pageurl.includes("TWOTOBHOME")) {
                initializePageObjects(browser, function () {
                    LandingPage.waitForElementVisible('@HBLOBContainer', data.averagewaittime)
                        .click('@HBLOBContainer')
                        .waitForElementVisible('@CurrentCoverage', data.averagewaittime)
                        .waitForElementVisible('@HomeButton', data.averagewaittime)
                        .click('@HomeButton')

                })
                console.log("verified SSO to HB");
            }
            else {
                initializePageObjects(browser, function () {
                    LandingPage.waitForElementVisible('@CurrentCoverage', data.averagewaittime)
                        .waitForElementVisible('@HomeButton', data.averagewaittime)
                        .click('@HomeButton');
                })
                console.log("verified SSO to HB");
            }
        });
    });

    this.When(/^User clicks on Your Profile button$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            LandingPage.waitForElementVisible('@YourProfileButton', data.averagewaittime)
                .click('@YourProfileButton');

        });
    });
    this.Then(/^Verify User gets redirected to Your Profile Page$/, function () {
        browser = this;
        initializePageObjects(browser, function () {

            ProfilePage.waitForElementVisible('@PersonalInformation', data.averagewaittime)
                .waitForElementVisible('@LoginInformation', data.averagewaittime)
                .waitForElementVisible('@SecurityQuestions', data.averagewaittime)

        });
    });

    this.When(/^User clicks on Help button$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            LandingPage.waitForElementVisible('@HelpButton', data.averagewaittime)
                .click('@HelpButton');

        });
    });
    this.Then(/^Verify User gets redirected to Help Page$/, function () {
        browser = this;
        initializePageObjects(browser, function () {

            LandingPage.waitForElementVisible('@HelpPageTitle', data.averagewaittime)
                    .waitForElementVisible('@HomeButton', data.averagewaittime)
                .click('@HomeButton');
        });
    });

    this.When(/^User clicks on Contact Us button$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            LandingPage.waitForElementVisible('@ContactUsButton', data.averagewaittime)
                .click('@ContactUsButton');

        });
    });
    this.Then(/^Verify User gets redirected to Contact Us Page$/, function () {
        browser = this;
        initializePageObjects(browser, function () {

            LandingPage.waitForElementVisible('@ContactUsPageTitle', data.averagewaittime)
                     .waitForElementVisible('@HomeButton', data.averagewaittime)
                .click('@HomeButton');
        });
    });

    this.When(/^User clicks on Privacy link$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            LandingPage.waitForElementVisible('@PrivacyLink', data.averagewaittime)
                .click('@PrivacyLink');

        });
    });
    this.Then(/^Verify User gets redirected to Privacy Policy Page$/, function () {
        browser = this;
        initializePageObjects(browser, function () {

            LandingPage.waitForElementVisible('@PrivacyPageTitle', data.averagewaittime)
                .waitForElementVisible('@HomeButton', data.averagewaittime)
                .click('@HomeButton');
        });
    });

    this.When(/^User clicks on Terms link$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            LandingPage.waitForElementVisible('@TermsLink', data.averagewaittime)
                .click('@TermsLink');

        });
    });
    this.Then(/^Verify User gets redirected to Terms of use Page$/, function () {
        browser = this;
        initializePageObjects(browser, function () {

            LandingPage.waitForElementVisible('@TermsPageTitle', data.averagewaittime)
                 .waitForElementVisible('@HomeButton', data.averagewaittime)
                .click('@HomeButton');
        });
    });

    this.When(/^User clicks on Full Website link$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            LandingPage.waitForElementVisible('@FullWebsiteLink', data.averagewaittime)
                .click('@FullWebsiteLink');

        });
    });
    this.Then(/^Verify User gets redirected to desktop IBCG2 Page$/, function () {
        browser = this;
        browser.url(function (current_url)
        {
            var pageurl = current_url.value
            var pageurl = pageurl.toUpperCase();
            if (!pageurl.includes("M-"))
            {
                console.log("successfully redirected to desktop site");
            }
            else
            {
                browser.assert.fail();
            }
            browser.back();
        })
    });

    this.When(/^User clicks on Logout button$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            LandingPage.waitForElementVisible('@LogOutButton', data.averagewaittime)
                .click('@LogOutButton');
            LogoutPage.waitForElementVisible('@PopupContent', data.averagewaittime)
                .waitForElementVisible('@ConfirmLogout', data.averagewaittime)
                .click('@ConfirmLogout')
        });
    });

    this.Then(/^User should able to logout successfully from IBCG2 mobile$/, function () {
         browser = this;
            initializePageObjects(browser, function () {

                LogoutPage.waitForElementVisible('@LogoutSuccess',data.averagewaittime);
                LogoutPage.waitForElementVisible('@LogInButton',data.averagewaittime)
                .click('@LogInButton');
            });

    })

};
