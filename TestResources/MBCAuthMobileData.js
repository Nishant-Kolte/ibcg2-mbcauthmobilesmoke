module.exports = {

    //Specify Your Testing Environment

       TestingEnvironment: 'QAI',
    // TestingEnvironment: 'CITEST',
  //   TestingEnvironment: 'CSO',
  //  TestingEnvironment: 'PROD',
    //  TestingEnvironment: 'CIDEV',


    urlQAI: 'https://m-auth-qai.mercerbenefitscentral.com/MBCQA3/login.tpz',
    urlCITEST: 'https://m-auth-cit.mercerbenefitscentral.com/MBCTRA/Login.tpz',
    urlCSO: 'https://m-auth-cso.mercerbenefitscentral.com/MBCTRA/Login.tpz',
    urlPROD: 'https://m-auth.mercerbenefitscentral.com/MBCTRA/Login.tpz',


    shortpause: 2000,
    averagepause: 7000,
    longpause: 30000,
    shortwaittime: 5000,
    averagewaittime: 40000,
    longwaittime: 120000
    ,
    userQAI: {
            username: 'mbcqa32600',
            password: 'Test0001' ,
    },
    userCITEST: {
            username: 'MBCTRA0005',
            password: 'MBCTRApass1',
    },
    userCSO: {
            username: 'MBCTRA0007',
            password: 'MBCTRApass1',
    },
    userPROD: {
        IBCUSER: {
            username: 'MBCTRA0002',
            password: 'MBCTRAPASS1' },
    },

}
