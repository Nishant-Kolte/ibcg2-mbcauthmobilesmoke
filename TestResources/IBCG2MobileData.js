module.exports = {

    //Specify Your Testing Environment

       TestingEnvironment: 'QAI',
    // TestingEnvironment: 'CITEST',
  //   TestingEnvironment: 'CSO',
  //  TestingEnvironment: 'PROD',
    //  TestingEnvironment: 'CIDEV',

    //urlQAI: 'https://m-qai.ibenefitcenter.com/QA01CL/login.tpz', //QA01CL
    urlQAI: 'https://m-qai.ibenefitcenter.com/login.tpz', //QALIFE
    urlCITEST: 'https://m-ctesti.ibenefitcenter.com/SLSDM/login.tpz',
    urlCSO: 'https://m-csoi.ibenefitcenter.com/SLSDM/login.tpz',
    urlPROD: 'https://m-ibenefitcenter2.mercerhrs.com/SLSDM/login.tpz',
    urlCIDEV: 'https://m-cdevi.ibenefitcenter.com/login.tpz',

    shortpause: 2000,
    averagepause: 7000,
    longpause: 30000,
    shortwaittime: 5000,
    averagewaittime: 40000,
    longwaittime: 120000
    ,
    userQAI: {
            // username: 'test025541270', //QA01CL
            // password: 'test0001' ,

            username: 'Test501010', //QALIFE
            password: 'TEST0001' ,
    },
    userCITEST: {
            username: 'testqa1201',
            password: 'test0001',
    },
    userCSO: {
            username: 'test1001',
            password: 'Test0001',
    },
    userPROD: {
            username: 'testqa1002',
            password: 'test0001',
    },
    userCIDEV: {
        IBCUSER: {
            username: 'testqa1002',
            password: 'Test0001' },
    },

}
