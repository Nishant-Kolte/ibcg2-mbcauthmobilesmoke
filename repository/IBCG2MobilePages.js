module.exports = {
    sections:
        {
            LoginPage:
                {
                    selector: 'body',
                    elements:
                        {
                            BenefitCentralHeader: {selector: "span.productname"},
                            LoginToYourAccount: {selector: "span[title='Log In to Your Account']"},
                            Username: {selector: "input[placeholder='User Name']"},
                            Password: {selector: "input[placeholder='Password']"},
                            Enter: {selector: "input[value='Enter']"},
                        },
                },
            LandingPage:
                {
                    selector: 'body',
                    elements:
                        {
                            CurrentCoverage: {
                            locateStrategy: 'xpath', selector: "//a[contains(text(),'Current Benefits')]"},
                            LogOutButton: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Log Out')]"},
                            HomeButton: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Home')]"},
                            YourProfileButton: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Your Profile')]"},
                            HelpButton: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Help')]"},
                            HelpPageTitle: {locateStrategy: 'xpath', selector: "//h1[contains(text(),'Help')]"},
                            ContactUsButton: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Contact Us')]"},
                            ContactUsPageTitle: {locateStrategy: 'xpath', selector: "//h1[contains(text(),'Contact Us')]"},
                            PrivacyLink: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Privacy')]"},
                            PrivacyPageTitle: {locateStrategy: 'xpath', selector: "//h1[contains(text(),'Privacy')]"},
                            TermsLink: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Terms')]"},
                            FullWebsiteLink: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Full Website')]"},
                            TermsPageTitle: {locateStrategy: 'xpath', selector: "//h1[contains(text(),'Terms')]"},
                            TwotobHome: {locateStrategy: 'xpath', selector: "//h1[contains(text(),'Benefits Home')]"},
                            OutagePage: {locateStrategy: 'xpath', selector: "//h1[contains(text(),'Outage')]"},
                            HBLOBContainer: {locateStrategy: 'xpath', selector: "//td[text()='Health']"},

                        },
                },
            ProfilePage:
                {
                    selector: 'body',
                    elements:
                        {
                            PersonalInformation: {locateStrategy: 'xpath', selector: "//h3[contains(text(),'Personal Information')]"},
                            LoginInformation: {locateStrategy: 'xpath', selector: "//h3[contains(text(),'Login Information')]"},
                            //UserNameField: {locateStrategy: 'xpath', selector: "//tbody/tr/td/div[3]/div/ul/li[@class='formdata clearleft'][1]"},
                            SecurityQuestions: {locateStrategy: 'xpath', selector: "//h3[contains(text(),'Security Questions')]"},

                        },
                },
            LogoutPage:
                {
                    selector: 'body',
                    elements:

                        {
                            PopupContent: {selector: "div.popup_content"},
                            ConfirmLogout: {selector: "input.button.primarybutton"},
                            LogoutSuccess: {locateStrategy: 'xpath', selector: "//p[contains(text(),'You have been successfully logged out of your account.')]"},
                            LogInButton: {locateStrategy: 'xpath', selector: "//input[@value='Log In']"},

                        },

                },
        }
};
