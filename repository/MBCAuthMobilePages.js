module.exports = {
    sections:
        {
            LoginPage:
                {
                    selector: 'body',
                    elements:
                        {
                           // MBCHeader: {selector: "span.productname"},
                            LoginToYourAccount: {selector: "span[title='Log In to Your Account']"},
                            Username: {selector: "input[placeholder='User Name']"},
                            Password: {selector: "input[placeholder='Password']"},
                            Enter: {selector: "input[value='Enter']"},
                        },
                },
            DashboardPage:
                {
                    selector: 'body',
                    elements:
                        {
                            WelcomeMsg: {locateStrategy: 'xpath', selector: "//h4[contains(text(),'Welcome back')]"},
                            LogoutLink: {locateStrategy: 'xpath', selector: "//span[contains(text(),'Logout')]/.."},
                            SecuritySettingsLink1: {locateStrategy: 'xpath', selector: "//span[contains(text(),'Update Security Settings')]"},
                            SecuritySettingsLink2: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Click here')]"},
                            Dashboardlink: {locateStrategy: 'xpath', selector: "//a[@href='/dashboard'][1]"},

                        },
                },
            YourProfilePage:
                {
                    selector: 'body',
                    elements:
                        {
                            ProfilePageTitle: {locateStrategy: 'xpath', selector: "//h1[contains(text(),'Your Profile')]"},
                            PersonalInformation: {locateStrategy: 'xpath', selector: "//h3[contains(text(),'Personal Information')]"},
                            LoginInformation: {locateStrategy: 'xpath', selector: "//h3[contains(text(),'Login Information')]"},
                            SecurityQuestions: {locateStrategy: 'xpath', selector: "//h3[contains(text(),'Security Questions')]"},
                        },
                },
            LogoutPage:
                {
                    selector: 'body',
                    elements:

                        {
                            ConfirmLogout: {selector: "input.button.primarybutton"},
                            LogoutSuccess: {locateStrategy: 'xpath', selector: "//p[contains(text(),'You have been successfully logged out of your account.')]"},
                            LogInButton: {locateStrategy: 'xpath', selector: "//input[@value='Log In']"},

                        },

                },
        }
};
